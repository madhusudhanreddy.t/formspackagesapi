﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using BLL;
using System.IO;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading.Tasks;
using System.Threading;
using FastReport;
using FastReport.Export;
using FastReport.Utils;
using FastReport.Export.PdfSimple;
using FastReport.Table;

namespace FormsPackagesAPI.Controllers
{
    public class FormsAPIController : ApiController
    {
        private string strcon = ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString;
        private string Repos = WebConfigurationManager.AppSettings["Repos"];
        private string Outputlocation = @WebConfigurationManager.AppSettings["Outputlocation"];
        private string AnnualStmtPath = @WebConfigurationManager.AppSettings["AnnualStmtPath"];
        private string jwt = WebConfigurationManager.AppSettings["jwt"];
        private string laserFischeBaseAddress = WebConfigurationManager.AppSettings["laserFischeBaseAddress"];
        private string emailFrom = WebConfigurationManager.AppSettings["emailFrom"];
        private string emailServer = WebConfigurationManager.AppSettings["emailServer"];

        private string DateTimeStr = DateTime.UtcNow.ToString("yyyyMMddHHmmsss");
        private string UserFolder = DateTime.UtcNow.ToString("yyyyMMddHHmmsss") + "\\";

        private string PDFReportPath = AppDomain.CurrentDomain.BaseDirectory + "/PolicyPDF/";

        PackageMethods BLObj = new PackageMethods();

        [NonAction]
        public HttpResponseMessage GetMktMaterialFromLaserfische(string URL, string formName, string savePath)
        {
            string laserFischeURL = URL + "?DName=" + formName + "&Repos=" + Repos + "&Outputlocation=" + savePath;

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(laserFischeBaseAddress);

            client.DefaultRequestHeaders.Add("jwt", jwt);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            return client.GetAsync(laserFischeURL).Result;
        }

        [NonAction]
        public HttpResponseMessage GetAnnualStmtFromLaserfische(string URL, string policyNo, string savePath)
        {
            string laserFischeURL = URL + "?Path=" + AnnualStmtPath + "&PolicyNum=" + policyNo + "&Repository=" + Repos + "&OutLocation=" + savePath;

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(laserFischeBaseAddress);

            client.DefaultRequestHeaders.Add("jwt", jwt);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            return client.GetAsync(laserFischeURL).Result;
        }

        [NonAction]
        public HttpResponseMessage GetPDFFromLaserfische(string URL, string formName, string savePath)
        {
            string laserFischeURL = URL + "?DocuName=" + formName + "&Repos=" + Repos + "&Outputlocation=" + savePath;

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(laserFischeBaseAddress);

            client.DefaultRequestHeaders.Add("jwt", jwt);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            return client.GetAsync(laserFischeURL).Result;
        }

        [NonAction]
        public HttpResponseMessage GetPkgFromLaserfische(string URL, string pkgName, string savePath)
        {
            string laserFischeURL = URL + "?PackageName=" + pkgName + "&Repos=" + Repos + "&Outputlocation=" + savePath;

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(laserFischeBaseAddress);

            client.DefaultRequestHeaders.Add("jwt", jwt);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            return client.GetAsync(laserFischeURL).Result;
        }

        [NonAction]
        public FileInfo GetFileFromTLBSharePath(string TLBSharePath, string filename = null)
        {
            DirectoryInfo dirInfo = new DirectoryInfo(TLBSharePath);
            FileInfo file = null;

            if (dirInfo.GetFiles().Count() == 0)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("No file(s) found")
                };
                throw new HttpResponseException(resp);
            }
            else if (dirInfo.GetFiles().Count() > 1)
            {
                BLObj.zipFiles(TLBSharePath);

                file = dirInfo.GetFiles().Where(x => x.Extension == ".zip").FirstOrDefault();
            }
            else if (dirInfo.GetFiles().Count() == 1)
            {
                file = dirInfo.GetFiles().FirstOrDefault();
                if (filename != null)
                {
                    file = dirInfo.GetFiles(filename + '*').FirstOrDefault();
                    while (!file.Exists)
                        file = dirInfo.GetFiles(filename + '*').FirstOrDefault();
                }
            }

            return file;
        }

        [Route("ViewForm/{docName}")]
        [HttpGet]
        public string ViewForm(string docName)
        {
            PackageMethods BLObj = new PackageMethods();

            string fileSavePath = BLObj.GetFileSavePath(Outputlocation + UserFolder);

            HttpResponseMessage response1 = GetPDFFromLaserfische(WebConfigurationManager.AppSettings["GetFormByName"], docName, fileSavePath);

            //Thread.Sleep(20000);
            byte[] b = new byte[] { };

            if (response1.IsSuccessStatusCode)
            {
                HttpResponseMessage responseMsg = new HttpResponseMessage(HttpStatusCode.OK);

                //FileInfo file = GetFileFromTLBSharePath(fileSavePath);
                DirectoryInfo dirInfo = new DirectoryInfo(fileSavePath);
                FileInfo file = dirInfo.GetFiles().FirstOrDefault();

                if (file != null)
                {
                    using (Stream fs = file.Open(FileMode.Open))
                    {
                        BinaryReader br = new BinaryReader(fs);
                        b = br.ReadBytes((Int32)fs.Length);

                        fs.Flush();
                        fs.Close();
                    }

                    if (Convert.ToBoolean(WebConfigurationManager.AppSettings["IsDeleteUserFolder"]))
                        BLObj.DeleteUserFolder(Outputlocation + UserFolder);

                    return b.ToString();
                }
                else
                {
                    var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                    {
                        Content = new StringContent("File not found")
                    };
                    throw new HttpResponseException(resp);
                }
            }
            else
            {
                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("File not found")
                };
                throw new HttpResponseException(resp);
            }
        }

        [Route("DownloadCorpBrochure/{docName}")]
        [HttpGet]
        public IHttpActionResult DownloadCorpBrochure(string docName)
        {
            string fileSavePath = BLObj.GetFileSavePath(Outputlocation + UserFolder);

            HttpResponseMessage response1 = GetPDFFromLaserfische(WebConfigurationManager.AppSettings["GetCorpBrochureByName"], docName, fileSavePath);

            //Thread.Sleep(30000);

            if (response1.IsSuccessStatusCode)
            {
                var responseMsg = Request.CreateResponse(HttpStatusCode.OK);

                //FileInfo file = GetFileFromTLBSharePath(fileSavePath);
                DirectoryInfo dirInfo = new DirectoryInfo(fileSavePath);
                FileInfo file = dirInfo.GetFiles().FirstOrDefault();

                while (!file.Exists)
                    file = dirInfo.GetFiles().FirstOrDefault();

                if (file != null)
                {
                    responseMsg.Content = new StreamContent(new FileStream(fileSavePath + "\\" + file.Name, FileMode.Open, FileAccess.Read));
                    responseMsg.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                    responseMsg.Content.Headers.ContentDisposition.FileName = file.Name;
                    responseMsg.Content.Headers.ContentType = response1.Content.Headers.ContentType;

                    var response = ResponseMessage(responseMsg);

                    if (Convert.ToBoolean(WebConfigurationManager.AppSettings["IsDeleteUserFolder"]))
                        BLObj.DeleteUserFolder(Outputlocation + UserFolder);

                    return response;
                }
                else
                {
                    var resp = new HttpResponseMessage(HttpStatusCode.NoContent)
                    {
                        Content = new StringContent("File not found")
                    };
                    throw new HttpResponseException(resp);
                }
            }
            else
            {
                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("Laserfische API has failed")
                };
                throw new HttpResponseException(resp);
            }
        }

        [Route("DownloadMktMaterial/{dName}")]
        [HttpGet]
        public IHttpActionResult DownloadMktMaterial(string dName)
        {
            string fileSavePath = BLObj.GetFileSavePath(Outputlocation + UserFolder);

            HttpResponseMessage response1 = GetMktMaterialFromLaserfische(WebConfigurationManager.AppSettings["GetMktMaterialByName"], dName, fileSavePath);

            //Thread.Sleep(30000);

            if (response1.IsSuccessStatusCode)
            {
                var responseMsg = Request.CreateResponse(HttpStatusCode.OK);

                //FileInfo file = GetFileFromTLBSharePath(fileSavePath);
                DirectoryInfo dirInfo = new DirectoryInfo(fileSavePath);
                FileInfo file = dirInfo.GetFiles().FirstOrDefault();

                while (!file.Exists)
                    file = dirInfo.GetFiles().FirstOrDefault();

                if (file != null)
                {
                    responseMsg.Content = new StreamContent(new FileStream(fileSavePath + "\\" + file.Name, FileMode.Open, FileAccess.Read));
                    responseMsg.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                    responseMsg.Content.Headers.ContentDisposition.FileName = file.Name;
                    responseMsg.Content.Headers.ContentType = response1.Content.Headers.ContentType;

                    var response = ResponseMessage(responseMsg);

                    if (Convert.ToBoolean(WebConfigurationManager.AppSettings["IsDeleteUserFolder"]))
                        BLObj.DeleteUserFolder(Outputlocation + UserFolder);

                    return response;
                }
                else
                {
                    var resp = new HttpResponseMessage(HttpStatusCode.NoContent)
                    {
                        Content = new StringContent("File not found")
                    };
                    throw new HttpResponseException(resp);
                }
            }
            else
            {
                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("Laserfische API has failed")
                };
                throw new HttpResponseException(resp);
            }
        }


        [Route("DownloadAnnualStmt/{fileName}/{policyNo}")]
        [HttpGet]
        public IHttpActionResult DownloadAnnualStmt(string fileName, string policyNo)
        {
            string fileSavePath = BLObj.GetFileSavePath(Outputlocation + UserFolder);

            HttpResponseMessage response1 = GetAnnualStmtFromLaserfische(WebConfigurationManager.AppSettings["GetAnnualStmt"], policyNo, fileSavePath);

            //Thread.Sleep(30000);

            if (response1.IsSuccessStatusCode)
            {
                var responseMsg = Request.CreateResponse(HttpStatusCode.OK);

                FileInfo file = GetFileFromTLBSharePath(fileSavePath, fileName);

                if (file != null)
                {
                    responseMsg.Content = new StreamContent(new FileStream(fileSavePath + "\\" + file.Name, FileMode.Open, FileAccess.Read));
                    responseMsg.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                    responseMsg.Content.Headers.ContentDisposition.FileName = file.Name;
                    responseMsg.Content.Headers.ContentType = response1.Content.Headers.ContentType;

                    var response = ResponseMessage(responseMsg);

                    if (Convert.ToBoolean(WebConfigurationManager.AppSettings["IsDeleteUserFolder"]))
                        BLObj.DeleteUserFolder(Outputlocation + UserFolder);

                    return response;
                }
                else
                {
                    var resp = new HttpResponseMessage(HttpStatusCode.NoContent)
                    {
                        Content = new StringContent("File not found")
                    };
                    throw new HttpResponseException(resp);
                }
            }
            else
            {
                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("Laserfische API has failed")
                };
                throw new HttpResponseException(resp);
            }
        }

        [Route("DownloadForm/{docName}")]
        [HttpGet]
        public IHttpActionResult DownloadForm(string docName)
        {
            string fileSavePath = BLObj.GetFileSavePath(Outputlocation + UserFolder);

            HttpResponseMessage response1 = GetPDFFromLaserfische(WebConfigurationManager.AppSettings["GetFormByName"], docName, fileSavePath);

            if (response1.IsSuccessStatusCode)
            {
                var responseMsg = Request.CreateResponse(HttpStatusCode.OK);

                //FileInfo file = GetFileFromTLBSharePath(fileSavePath);
                DirectoryInfo dirInfo = new DirectoryInfo(fileSavePath);
                FileInfo file = dirInfo.GetFiles().FirstOrDefault();

                if (file != null)
                {
                    responseMsg.Content = new StreamContent(new FileStream(fileSavePath + "\\" + file.Name, FileMode.Open, FileAccess.Read));
                    responseMsg.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                    responseMsg.Content.Headers.ContentDisposition.FileName = file.Name;
                    responseMsg.Content.Headers.ContentType = response1.Content.Headers.ContentType;

                    var response = ResponseMessage(responseMsg);

                    if (Convert.ToBoolean(WebConfigurationManager.AppSettings["IsDeleteUserFolder"]))
                        BLObj.DeleteUserFolder(Outputlocation + UserFolder);

                    return response;
                }
                else
                {
                    var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                    {
                        Content = new StringContent("File not found")
                    };
                    throw new HttpResponseException(resp);
                }
            }
            else
            {
                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("File not found")
                };
                throw new HttpResponseException(resp);
            }
        }

        [Route("DownloadForms")]
        [HttpGet]
        public HttpResponseMessage DownloadForms([FromUri] string[] docNames)
        {
            string errorPkgs = String.Empty;
            string errorForms = String.Empty;
            string errorRespContent = String.Empty;
            string fileSavePath = BLObj.GetFileSavePath(Outputlocation + UserFolder);

            try
            {
                if (!ModelState.IsValid)
                {
                    var response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid data.");
                    return response;
                }

                foreach (string docName in docNames)
                {
                    HttpResponseMessage response1 = GetPDFFromLaserfische(WebConfigurationManager.AppSettings["GetFormByName"], docName, fileSavePath);

                    if (!response1.IsSuccessStatusCode)
                        errorRespContent += "File " + docName + " is not found in Laserfische </br>";
                }

                HttpResponseMessage responseMsg = new HttpResponseMessage(HttpStatusCode.OK);

                FileInfo file = GetFileFromTLBSharePath(fileSavePath);

                if (file != null)
                {
                    responseMsg.Content = new StreamContent(new FileStream(fileSavePath + "\\" + file.Name, FileMode.Open, FileAccess.Read));
                    if (errorRespContent != String.Empty)
                        responseMsg.Content.Headers.Add("MissingLaserFischefiles", errorRespContent);
                    responseMsg.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                    responseMsg.Content.Headers.ContentDisposition.FileName = file.Name;
                    responseMsg.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

                    if (Convert.ToBoolean(WebConfigurationManager.AppSettings["IsDeleteUserFolder"]))
                        BLObj.DeleteUserFolder(Outputlocation + UserFolder);

                    return responseMsg;
                }
                else
                {
                    var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                    {
                        Content = new StringContent("No File(s) found")
                    };
                    throw new HttpResponseException(resp);
                }
            }
            catch (HttpResponseException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Response.Content.ReadAsStringAsync().Result);
            }
        }

        //POST: api/DownloadDefaultPackage
        //[Authorize]
        [Route("DownloadDefaultPackage")]
        [HttpGet]
        public HttpResponseMessage DownloadDefaultPackage([FromUri] List<DefaultPackage> ListPackages)
        {
            string errorPkgs = String.Empty;
            string errorRespContent = String.Empty;
            string fileSavePath = BLObj.GetFileSavePath(Outputlocation + UserFolder);

            if (!ModelState.IsValid)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("Invalid data.")
                };
                throw new HttpResponseException(resp);
            }
            try
            {
                foreach (DefaultPackage pkg in ListPackages)
                {
                    HttpResponseMessage response1 = GetPkgFromLaserfische(WebConfigurationManager.AppSettings["GetPackageByName"], pkg.Package_Name, fileSavePath);

                    if (!response1.IsSuccessStatusCode)
                    {
                        var resp = new HttpResponseMessage(HttpStatusCode.NoContent)
                        {
                            Content = new StringContent(errorPkgs),
                            ReasonPhrase = "Cannot download one or more packages as they are not found"
                        };
                        errorRespContent = resp.Content.ToString();
                    }
                }

                HttpResponseMessage responseMsg = new HttpResponseMessage(HttpStatusCode.OK);

                FileInfo file = GetFileFromTLBSharePath(fileSavePath);

                if (file != null)
                {
                    responseMsg.Content = new StreamContent(new FileStream(fileSavePath + "\\" + file.Name, FileMode.Open, FileAccess.Read));
                    if (errorRespContent != String.Empty)
                        responseMsg.Content.Headers.Add("MissingLaserFischefiles", errorRespContent);
                    responseMsg.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                    responseMsg.Content.Headers.ContentDisposition.FileName = file.Name;
                    responseMsg.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

                    if (Convert.ToBoolean(WebConfigurationManager.AppSettings["IsDeleteUserFolder"]))
                        BLObj.DeleteUserFolder(Outputlocation + UserFolder);

                    return responseMsg;
                }
                else
                {
                    var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                    {
                        Content = new StringContent("No File(s) found")
                    };
                    throw new HttpResponseException(resp);
                }
            }
            catch (HttpResponseException ex)
            {
                throw;
            }
        }

        //POST: api/DownloadCustomPackage
        //[Authorize]
        [Route("DownloadCustomPackage")]
        [HttpGet]
        public HttpResponseMessage DownloadCustomPackage([FromUri] PackagesAndFormsNames ListPackagesForms)
        {
            string errorPkgs = String.Empty;
            string errorForms = String.Empty;
            string errorRespContent = String.Empty;
            string fileSavePath = BLObj.GetFileSavePath(Outputlocation + UserFolder);

            if (!ModelState.IsValid)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("Invalid data.")
                };
                throw new HttpResponseException(resp);
            }
            try
            {
                if (ListPackagesForms.FullPackageIDs != null)
                {
                    List<Forms> CPFormsList = new List<Forms> { };

                    foreach (int pkg in ListPackagesForms.FullPackageIDs)
                    {
                        CustomPackage updatedPkg = new CustomPackage();
                        updatedPkg = BLObj.GetPackageForms(pkg);

                        if (updatedPkg.PackageID == 0)
                            errorPkgs += string.Format("Custom package with ID {0} is not found. ", pkg);
                        else
                            CPFormsList.AddRange(updatedPkg.FormsList);

                        foreach (Forms CPform in CPFormsList)
                        {
                            HttpResponseMessage response1 = GetPDFFromLaserfische(WebConfigurationManager.AppSettings["GetFormByName"], CPform.Form_Name, fileSavePath);

                            if (!response1.IsSuccessStatusCode)
                                errorForms += string.Format("Form {0} is not found. ", CPform);
                        }
                    }
                }

                if (ListPackagesForms.PartialPkg != null)
                    foreach (CustomPackage PartialPkgFormsList in ListPackagesForms.PartialPkg)
                    {
                        foreach (Forms form in PartialPkgFormsList.FormsList)
                        {
                            HttpResponseMessage response1 = GetPDFFromLaserfische(WebConfigurationManager.AppSettings["GetFormByName"], form.Form_Name, fileSavePath);

                            if (!response1.IsSuccessStatusCode)
                                errorForms += string.Format("Form {0} is not found. ", form);
                        }
                    }

                if (errorPkgs != String.Empty || errorForms != String.Empty)
                {
                    var resp = new HttpResponseMessage(HttpStatusCode.NoContent)
                    {
                        Content = new StringContent(errorPkgs + errorForms),
                        ReasonPhrase = "Cannot download one or more packages/forms as they are not found"
                    };
                    errorRespContent = resp.Content.ToString();
                }
                HttpResponseMessage responseMsg = new HttpResponseMessage(HttpStatusCode.OK);

                FileInfo file = GetFileFromTLBSharePath(fileSavePath);

                if (file != null)
                {
                    responseMsg.Content = new StreamContent(new FileStream(fileSavePath + "\\" + file.Name, FileMode.Open, FileAccess.Read));
                    if (errorRespContent != String.Empty)
                        responseMsg.Content.Headers.Add("MissingLaserFischefiles", errorRespContent);
                    responseMsg.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                    responseMsg.Content.Headers.ContentDisposition.FileName = file.Name;
                    responseMsg.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

                    if (Convert.ToBoolean(WebConfigurationManager.AppSettings["IsDeleteUserFolder"]))
                        BLObj.DeleteUserFolder(Outputlocation + UserFolder);

                    return responseMsg;
                }
                else
                {
                    var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                    {
                        Content = new StringContent("No File(s) found")
                    };
                    throw new HttpResponseException(resp);
                }
            }
            catch (HttpResponseException ex)
            {
                throw;
            }
        }

        //[Route("GetFormsByCountry/{CountryCode}")]
        //[HttpGet]
        //public IHttpActionResult GetFormsByCountry(string CountryCode)
        //{
        //    IHttpActionResult response;

        //    string fileSavePath = BLObj.GetFileSavePath(Outputlocation + UserFolder);

        //    string laserFischeURL = WebConfigurationManager.AppSettings["GetFormByCountry"] + "?Country =" + CountryCode + "&Repos=" + Repos + "&Outputlocation=" + fileSavePath;

        //    HttpClient client = new HttpClient();
        //    client.BaseAddress = new Uri(laserFischeBaseAddress);

        //    client.DefaultRequestHeaders.Add("jwt", jwt);
        //    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        //    HttpResponseMessage response1 = client.GetAsync(laserFischeURL).Result;

        //    if (response1.IsSuccessStatusCode)
        //    {
        //        BLObj.zipFiles(fileSavePath);

        //        DirectoryInfo dirInfo = new DirectoryInfo(fileSavePath);

        //        HttpResponseMessage responseMsg = new HttpResponseMessage(HttpStatusCode.OK);

        //        foreach (FileInfo file in dirInfo.GetFiles())
        //        {
        //            responseMsg.Content = new StreamContent(new FileStream(file.Name, FileMode.Open, FileAccess.Read));
        //            responseMsg.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
        //            responseMsg.Content.Headers.ContentDisposition.FileName = file.Name;
        //            responseMsg.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
        //        }
        //        response = ResponseMessage(responseMsg);

        //        return response;
        //    }
        //    else
        //    {
        //        var resp = new HttpResponseMessage(HttpStatusCode.Unauthorized)
        //        {
        //            Content = new StringContent("Files are not available")
        //        };
        //        throw new HttpResponseException(resp);
        //    }
        //}

        // GET: api/GetAllPackages/{useremail}
        [Route("GetAllPackages/{useremail}")]
        [HttpGet]
        public List<CustomPackageGetAll> GetAllPackages(string useremail)
        {
            //List<CustomPackage> pkglist = new List<CustomPackage>();

            var pkglist = (from pkg in BLObj.GetAllPackages(useremail)
                           orderby pkg.LastUpdatedDate descending
                           select pkg).ToList();

            if (pkglist.Count == 0)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent("Custom packages not found"),
                    ReasonPhrase = "Currently there are no custom packages"
                };
                throw new HttpResponseException(resp);
            }
            return pkglist;
        }

        [Route("GetCustomPackageDetail/{packageID}")]
        [HttpGet]
        public CustomPackage GetCustomPackage(int packageID)
        {
            CustomPackage pkg = new CustomPackage();

            pkg = BLObj.GetPackageForms(packageID);

            if (pkg.PackageID == 0)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.NoContent)
                {
                    Content = new StringContent(string.Format("Custom package with id {0} is not found. ", packageID))
                };
                throw new HttpResponseException(resp);
            }
            else
            {
                return pkg;
            }
        }

        // POST: api/AddCustomPackage
        //[Authorize]
        [Route("AddCustomPackage")]
        [HttpPost]
        public IHttpActionResult PostCP([FromBody] CustomPackage Pkg)
        {
            IHttpActionResult response;

            if (!ModelState.IsValid)
            {
                response = ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid data."));
                return response;
            }
            try
            {
                //Pkg.CreatedBy = "TestUser"; //User.Identity.Name;

                string result = BLObj.AddCustomPackage(Pkg);
                int respCode;

                if (result == "Custom package created successfully")
                    respCode = Convert.ToInt32(HttpStatusCode.OK);
                else
                    respCode = Convert.ToInt32(HttpStatusCode.BadRequest);

                var responseMsg = Request.CreateResponse((HttpStatusCode)respCode, result);

                responseMsg.Headers.Location = new Uri(Request.RequestUri.ToString());
                response = ResponseMessage(responseMsg);

                return response;
            }
            catch (HttpResponseException ex)
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Response.Content.ReadAsStringAsync().Result));
            }
        }

        // POST: api/UpdateCustomPackage/{packageID}
        //[Authorize]
        [Route("UpdateCustomPackage/{packageID}")]
        [HttpPut]
        public IHttpActionResult PutCP(int packageID, [FromBody] CustomPackage Pkg)
        {
            IHttpActionResult response;
            CustomPackage updatedPkg = new CustomPackage();

            if (!ModelState.IsValid)
            {
                response = ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid data."));
                return response;
            }
            try
            {
                updatedPkg = BLObj.GetPackageForms(packageID);

                if (updatedPkg.PackageID == 0)
                {
                    var resp = new HttpResponseMessage(HttpStatusCode.NoContent)
                    {
                        Content = new StringContent(string.Format("Custom package with id {0} is not found. ", packageID)),
                        ReasonPhrase = "Cannot update as package is not found"
                    };
                    throw new HttpResponseException(resp);
                }

                string result = BLObj.UpdateCustomPackage(Pkg);
                int respCode;

                if (result == "Forms are updated successfully to the selected Package")
                    respCode = Convert.ToInt32(HttpStatusCode.OK);
                else
                    respCode = Convert.ToInt32(HttpStatusCode.BadRequest);

                var responseMsg = Request.CreateResponse((HttpStatusCode)respCode, result);

                responseMsg.Headers.Location = new Uri(Request.RequestUri.ToString());
                response = ResponseMessage(responseMsg);

                return response;
            }
            catch (HttpResponseException ex)
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Response.Content.ReadAsStringAsync().Result));
            }
        }

        // POST: api/DeleteCustomPackage
        //[Authorize]
        [Route("DeleteCustomPackage")]
        [HttpPut]
        public IHttpActionResult DeleteCP([FromBody] PackagesAndFormsNames ListPackagesForms)
        {
            IHttpActionResult response;
            string errorPkgs = String.Empty;
            string errorForms = String.Empty;
            string errorRespContent = String.Empty;
            string fileSavePath = BLObj.GetFileSavePath(Outputlocation + UserFolder);

            if (!ModelState.IsValid)
            {
                response = ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid data."));
                return response;
            }
            try
            {
                List<CustomPackage> PackageFormsList = new List<CustomPackage> { };

                if (ListPackagesForms.FullPackageIDs != null)
                    foreach (int pkg in ListPackagesForms.FullPackageIDs)
                    {
                        CustomPackage updatedPkg = new CustomPackage();
                        updatedPkg = BLObj.GetPackageForms(pkg);

                        if (updatedPkg.PackageID == 0)
                            errorPkgs += string.Format("Custom package with ID {0} is not found. ", pkg);
                        else
                        {
                            updatedPkg.FormsList = null;
                            PackageFormsList.Add(updatedPkg);
                        }
                    }

                if (ListPackagesForms.PartialPkg != null)
                    foreach (CustomPackage PartialPkgFormsList in ListPackagesForms.PartialPkg)
                    {
                        foreach (Forms form in PartialPkgFormsList.FormsList)
                        {
                            HttpResponseMessage response1 = GetPDFFromLaserfische(WebConfigurationManager.AppSettings["GetFormByName"], form.Form_Name, fileSavePath);

                            if (!response1.IsSuccessStatusCode)
                                errorForms += string.Format("Form {0} is not found. ", form);
                        }
                        PackageFormsList.Add(PartialPkgFormsList);

                        if (errorPkgs != String.Empty || errorForms != String.Empty)
                        {
                            var resp = new HttpResponseMessage(HttpStatusCode.NoContent)
                            {
                                Content = new StringContent(errorPkgs + errorForms),
                                ReasonPhrase = "Cannot delete as one or more packages/forms are not found"
                            };
                            errorRespContent = resp.Content.ToString();
                        }
                    }

                string result = BLObj.DeleteMultiPackagesForms(PackageFormsList);
                int respCode;

                if (result == "Custom package deleted successfully")
                    respCode = Convert.ToInt32(HttpStatusCode.OK);
                else
                    respCode = Convert.ToInt32(HttpStatusCode.BadRequest);

                var responseMsg = Request.CreateResponse((HttpStatusCode)respCode, result);

                if (errorRespContent != String.Empty)
                    responseMsg.Content.Headers.Add("MissingLaserFischefiles", errorRespContent);
                responseMsg.Headers.Location = new Uri(Request.RequestUri.ToString());
                response = ResponseMessage(responseMsg);

                return response;
            }
            catch (HttpResponseException ex)
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Response.Content.ReadAsStringAsync().Result));
            }
        }

        //POST: api/SendEmailDefaultPackages
        //[Authorize]
        [Route("SendEmailDefaultPackages")]
        [HttpGet]
        public IHttpActionResult SendEmailDefaultPackages([FromUri] EmailContents email)
        {
            string errorPkgs = String.Empty;
            string errorForms = String.Empty;
            string errorRespContent = String.Empty;
            string fileSavePath = BLObj.GetFileSavePath(Outputlocation + UserFolder);

            string recipient = email.recipient;
            MailMessage message = new MailMessage(emailFrom, recipient);
            message.Subject = email.Subject;
            message.Body = email.Body;
            //message.Bcc.Add(WebConfigurationManager.AppSettings["BCC"]);

            SmtpClient client = new SmtpClient(emailServer);

            if (!ModelState.IsValid)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("Invalid data.")
                };
                throw new HttpResponseException(resp);
            }
            try
            {
                if (email.DefaultPkgList != null)
                {
                    foreach (DefaultPackage pkg in email.DefaultPkgList)
                    {
                        HttpResponseMessage response1 = GetPkgFromLaserfische(WebConfigurationManager.AppSettings["GetPackageByName"], pkg.Package_Name, fileSavePath);

                        if (!response1.IsSuccessStatusCode)
                        {
                            var resp = new HttpResponseMessage(HttpStatusCode.NoContent)
                            {
                                Content = new StringContent(errorPkgs),
                                ReasonPhrase = "Cannot download one or more packages as they are not found"
                            };
                            errorRespContent = resp.Content.ToString();
                        }
                    }

                    HttpResponseMessage responseMsg = new HttpResponseMessage(HttpStatusCode.OK);

                    DirectoryInfo dirInfo = new DirectoryInfo(fileSavePath);
                    FileInfo file = GetFileFromTLBSharePath(fileSavePath);

                    if (file == null)
                    {
                        var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                        {
                            Content = new StringContent("No File(s) found")
                        };
                        throw new HttpResponseException(resp);
                    }
                    else if (file.Length > (20 * 1024 * 1024))
                    {
                        var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                        {
                            Content = new StringContent("Attachments cannot be greater than 20 MB")
                        };
                        throw new HttpResponseException(resp);
                    }

                    client.Credentials = new System.Net.NetworkCredential("", "");

                    System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(dirInfo.FullName + "/" + file.Name, MediaTypeNames.Application.Octet);
                    message.Attachments.Add(attachment);

                    client.Send(message);

                    if (Convert.ToBoolean(WebConfigurationManager.AppSettings["IsDeleteUserFolder"]))
                        BLObj.DeleteUserFolder(Outputlocation + UserFolder);
                }
                else
                {
                    var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                    {
                        Content = new StringContent("No Default packages are selected to email")
                    };
                    throw new HttpResponseException(resp);
                }

                return Ok("Email sent successfully"); ;
            }
            catch (HttpResponseException ex)
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Response.Content.ReadAsStringAsync().Result));
            }
        }

        //POST: api/SendEmailCustomPackagesForms
        //[Authorize]
        [Route("SendEmailCustomPackagesForms")]
        [HttpGet]
        public IHttpActionResult SendEmailCustomPackagesForms([FromUri] EmailContents email)
        {
            string errorPkgs = String.Empty;
            string errorForms = String.Empty;
            string errorRespContent = String.Empty;
            string fileSavePath = BLObj.GetFileSavePath(Outputlocation + UserFolder);

            string recipient = email.recipient;
            MailMessage message = new MailMessage(emailFrom, recipient);
            message.Subject = email.Subject;
            message.Body = email.Body;
            //message.Bcc.Add(WebConfigurationManager.AppSettings["BCC"]);

            SmtpClient client = new SmtpClient(emailServer);

            if (!ModelState.IsValid)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("Invalid data.")
                };
                throw new HttpResponseException(resp);
            }
            try
            {
                List<Forms> CPFormsList = new List<Forms> { };

                if (email.CustomPackagesForms != null)
                {
                    if (email.CustomPackagesForms.FullPackageIDs != null)
                        foreach (int pkg in email.CustomPackagesForms.FullPackageIDs)
                        {
                            CustomPackage updatedPkg = new CustomPackage();
                            updatedPkg = BLObj.GetPackageForms(pkg);

                            if (updatedPkg.PackageID == 0)
                                errorPkgs += string.Format("Custom package with ID {0} is not found. ", pkg);
                            else
                                CPFormsList.AddRange(updatedPkg.FormsList);

                            foreach (Forms CPform in CPFormsList)
                            {
                                HttpResponseMessage response1 = GetPDFFromLaserfische(WebConfigurationManager.AppSettings["GetFormByName"], CPform.Form_Name, fileSavePath);

                                if (!response1.IsSuccessStatusCode)
                                    errorForms += string.Format("Form {0} is not found. ", CPform);
                            }
                        }

                    if (email.CustomPackagesForms.PartialPkg != null)
                        foreach (CustomPackage PartialPkgFormsList in email.CustomPackagesForms.PartialPkg)
                        {
                            foreach (Forms form in PartialPkgFormsList.FormsList)
                            {
                                HttpResponseMessage response1 = GetPDFFromLaserfische(WebConfigurationManager.AppSettings["GetFormByName"], form.Form_Name, fileSavePath);

                                if (!response1.IsSuccessStatusCode)
                                    errorForms += string.Format("Form {0} is not found. ", form);
                            }
                        }

                    if (errorPkgs != String.Empty || errorForms != String.Empty)
                    {
                        var resp = new HttpResponseMessage(HttpStatusCode.NoContent)
                        {
                            Content = new StringContent(errorPkgs + errorForms),
                            ReasonPhrase = "Cannot fetch one or more packages/forms as they are not found"
                        };
                        errorRespContent = resp.Content.ToString();
                    }
                    HttpResponseMessage responseMsg = new HttpResponseMessage(HttpStatusCode.OK);

                    DirectoryInfo dirInfo = new DirectoryInfo(fileSavePath);
                    FileInfo file = GetFileFromTLBSharePath(fileSavePath);

                    if (file == null)
                    {
                        var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                        {
                            Content = new StringContent("No File(s) found")
                        };
                        throw new HttpResponseException(resp);
                    }
                    else if (file.Length > (20 * 1024 * 1024))
                    {
                        var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                        {
                            Content = new StringContent("Attachments cannot be greater than 20 MB")
                        };
                        throw new HttpResponseException(resp);
                    }

                    client.Credentials = new System.Net.NetworkCredential("", "");

                    System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(dirInfo.FullName + "/" + file.Name, MediaTypeNames.Application.Octet);
                    message.Attachments.Add(attachment);

                    client.Send(message);

                    if (Convert.ToBoolean(WebConfigurationManager.AppSettings["IsDeleteUserFolder"]))
                        BLObj.DeleteUserFolder(Outputlocation + UserFolder);
                }
                else
                {
                    var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                    {
                        Content = new StringContent("No Custom packages/Forms are selected to email")
                    };
                    throw new HttpResponseException(resp);
                }
                return Ok("Email sent successfully"); ;
            }
            catch (HttpResponseException ex)
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Response.Content.ReadAsStringAsync().Result));
            }
        }

        //POST: api/SendEmailForms
        //[Authorize]
        [Route("SendEmailForms")]
        [HttpGet]
        public IHttpActionResult SendEmailForms([FromUri] EmailContents email)
        {
            string errorPkgs = String.Empty;
            string errorForms = String.Empty;
            string errorRespContent = String.Empty;
            string fileSavePath = BLObj.GetFileSavePath(Outputlocation + UserFolder);

            string recipient = email.recipient;
            MailMessage message = new MailMessage(emailFrom, recipient);
            message.Subject = email.Subject;
            message.Body = email.Body;
            //message.Bcc.Add(WebConfigurationManager.AppSettings["BCC"]);

            SmtpClient client = new SmtpClient(emailServer);

            if (!ModelState.IsValid)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("Invalid data.")
                };
                throw new HttpResponseException(resp);
            }
            try
            {
                if (email.docNames != null)
                {
                    foreach (string docName in email.docNames)
                    {
                        HttpResponseMessage response1 = GetPDFFromLaserfische(WebConfigurationManager.AppSettings["GetFormByName"], docName, fileSavePath);

                        if (!response1.IsSuccessStatusCode)
                            errorRespContent += "File " + docName + " is not found in Laserfische </br>";
                    }

                    HttpResponseMessage responseMsg = new HttpResponseMessage(HttpStatusCode.OK);

                    DirectoryInfo dirInfo = new DirectoryInfo(fileSavePath);

                    FileInfo file = GetFileFromTLBSharePath(fileSavePath);

                    if (file == null)
                    {
                        var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                        {
                            Content = new StringContent("No File(s) found")
                        };
                        throw new HttpResponseException(resp);
                    }
                    else if (file.Length > (20 * 1024 * 1024))
                    {
                        var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                        {
                            Content = new StringContent("Attachments cannot be greater than 20 MB")
                        };
                        throw new HttpResponseException(resp);
                    }

                    client.Credentials = new System.Net.NetworkCredential("", "");

                    System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(dirInfo.FullName + "/" + file.Name, MediaTypeNames.Application.Octet);
                    message.Attachments.Add(attachment);

                    client.Send(message);

                    if (Convert.ToBoolean(WebConfigurationManager.AppSettings["IsDeleteUserFolder"]))
                        BLObj.DeleteUserFolder(Outputlocation + UserFolder);
                }
                else
                {
                    var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                    {
                        Content = new StringContent("No Forms are selected to email")
                    };
                    throw new HttpResponseException(resp);
                }

                return Ok("Email sent successfully"); ;
            }
            catch (HttpResponseException ex)
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Response.Content.ReadAsStringAsync().Result));
            }
        }

        //[Route("GeneratePDF")]
        //[HttpPost]
        //public IHttpActionResult GeneratePDF([FromBody] PDFReport policyDetails)
        //{
        //    PDFReport PDFObj = new BLL.PDFReport();

        //    Report report = new Report();

        //    Config.WebMode = true;
        //    report.Load(PDFReportPath + "PolicyDetails.frx");

        //    TableObject tblPlanInfo = (TableObject)report.FindObject("PlanInfo");
        //    TableObject tblBeneficiaries = (TableObject)report.FindObject("Beneficiaries");
        //    TableObject tblAddresses = (TableObject)report.FindObject("Addresses");

        //    report.SetParameterValue("PolicyNo", policyDetails.policyNo);
        //    report.SetParameterValue("policyDt", policyDetails.policyDt.Date);
        //    report.SetParameterValue("policyStat", policyDetails.policyStat);
        //    report.SetParameterValue("policyIssueState", policyDetails.policyIssueState);
        //    report.SetParameterValue("policyType", policyDetails.policyType);
        //    report.SetParameterValue("productName", policyDetails.productName);
        //    report.SetParameterValue("RCUC", policyDetails.RCUC);
        //    report.SetParameterValue("ratedPolicy", policyDetails.ratedPolicy);
        //    report.SetParameterValue("serviceOfc", policyDetails.serviceOfc);
        //    report.SetParameterValue("serviceAgent", policyDetails.serviceAgent);
        //    report.SetParameterValue("OwnerName", policyDetails.ownerName);
        //    report.SetParameterValue("OwnerGender", policyDetails.ownerGender);
        //    report.SetParameterValue("OwnerDOB", policyDetails.ownerDOB.Date);
        //    report.SetParameterValue("USPerson", policyDetails.USPerson);
        //    report.SetParameterValue("InsuredName", policyDetails.insuredName);
        //    report.SetParameterValue("InsuredGender", policyDetails.insuredGender);
        //    report.SetParameterValue("InsuredDOB", policyDetails.insuredDOB.Date);
        //    report.SetParameterValue("IssueAge", policyDetails.issueAge);
        //    report.SetParameterValue("plan", policyDetails.plan);
        //    report.SetParameterValue("tblRating", policyDetails.tblRating);
        //    report.SetParameterValue("sumAssured", policyDetails.sumAssured);
        //    report.SetParameterValue("maturityDt", policyDetails.maturityDt.Date);
        //    report.SetParameterValue("AnnCovPrem", policyDetails.AnnCovPrem);
        //    report.SetParameterValue("cashValue", policyDetails.cashValue);
        //    report.SetParameterValue("surrenderValue", policyDetails.surrenderValue);
        //    report.SetParameterValue("dividendOption", policyDetails.dividendOption);
        //    report.SetParameterValue("NonForfeitOpt", policyDetails.NonForfeitOpt);
        //    report.SetParameterValue("outStandLoan", policyDetails.outStandLoan);
        //    report.SetParameterValue("maxAvailableLoan", policyDetails.maxAvailableLoan);
        //    report.SetParameterValue("premDepFund", policyDetails.premDepFund);
        //    report.SetParameterValue("loanIntRate", policyDetails.loanIntRate);
        //    report.SetParameterValue("modalPremAmt", policyDetails.modalPremAmt);
        //    report.SetParameterValue("annPremium", policyDetails.annPremium);
        //    report.SetParameterValue("lastPremPayDt", policyDetails.lastPremPayDt);
        //    report.SetParameterValue("lastPremPayAmt", policyDetails.lastPremPayAmt);
        //    report.SetParameterValue("paidToDt", policyDetails.paidToDt);
        //    report.SetParameterValue("payFreq", policyDetails.payFreq);
        //    report.SetParameterValue("payMethod", policyDetails.payMethod);
        //    report.SetParameterValue("accNumber", policyDetails.accNumber);


        //    foreach (var item in policyDetails.beneficiaryList)
        //    {
        //        TableCell tcBenefName = new TableCell();
        //        TableCell tcrelation = new TableCell();
        //        TableCell tcpercent = new TableCell();

        //        tcBenefName.Text = item.name.ToString();
        //        tcrelation.Text = item.relation.ToString();
        //        tcpercent.Text = item.percentage.ToString();

        //        TableRow trbenef = new TableRow();

        //        trbenef.AddChild(tcBenefName);
        //        trbenef.AddChild(tcrelation);
        //        trbenef.AddChild(tcpercent);

        //        tblBeneficiaries.Rows.Add(trbenef);
        //    }

        //    foreach (var item in policyDetails.addressList)
        //    {
        //        TableCell tcRole = new TableCell();
        //        TableCell tcName = new TableCell();
        //        TableCell tcCorrepondAddress = new TableCell();
        //        TableCell tcContact = new TableCell();
        //        TableCell tcEmail = new TableCell();

        //        tcRole.Text = item.role.ToString();
        //        tcName.Text = item.name.ToString();
        //        tcCorrepondAddress.Text = item.correspondenceAddress.ToString();
        //        tcContact.Text = item.contact.ToString();
        //        tcEmail.Text = item.email.ToString();

        //        TableRow trAddress = new TableRow();

        //        trAddress.AddChild(tcRole);
        //        trAddress.AddChild(tcName);
        //        trAddress.AddChild(tcCorrepondAddress);
        //        trAddress.AddChild(tcContact);
        //        trAddress.AddChild(tcEmail);

        //        tblAddresses.Rows.Add(trAddress);
        //    }

        //    report.Prepare(false);

        //    PDFSimpleExport objPDFExport = new PDFSimpleExport();
        //    objPDFExport.OpenAfterExport = false;

        //    report.Export(objPDFExport, PDFReportPath + "//" + DateTimeStr + "PolicyDetails.pdf");

        //    DirectoryInfo dirInfo = new DirectoryInfo(PDFReportPath);
        //    FileInfo file = dirInfo.GetFiles(DateTimeStr + "PolicyDetails.pdf").FirstOrDefault();

        //    if (file != null)
        //    {
        //        var responseMsg = Request.CreateResponse(HttpStatusCode.OK);

        //        if (Convert.ToBoolean(WebConfigurationManager.AppSettings["IsDeleteUserFolder"]))
        //            BLObj.DeletePolicyPDF(PDFReportPath);

        //        responseMsg.Content = new StreamContent(new FileStream(PDFReportPath +"//"+ DateTimeStr + "PolicyDetails.pdf", FileMode.Open, FileAccess.Read));
        //        responseMsg.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
        //        responseMsg.Content.Headers.ContentDisposition.FileName = file.Name;
        //        responseMsg.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

        //        var response = ResponseMessage(responseMsg);

        //        return response;
        //    }
        //    else
        //    {
        //        var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
        //        {
        //            Content = new StringContent("File not found")
        //        };
        //        throw new HttpResponseException(resp);
        //    }
        //}
    }
}
