﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Configuration;

namespace DAL
{
    public class FormsPkgDL
    {
        private string strcon = ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString;

        public DataSet GetAllPackages(string CreatedBy)
        {
            SqlConnection DbConnection = new SqlConnection(strcon);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();

            try
            {
                DbConnection.Open();

                SqlCommand command = new SqlCommand("[PAM].[usp_GetCustomPackages]", DbConnection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 100).Value = CreatedBy;

                da.SelectCommand = command;
                da.Fill(ds);

                DbConnection.Close();
                return ds;
            }
            catch (SqlException ex)
            {
                throw;
            }
        }

        public DataSet GetPackageForms(int PackageID)
        {
            SqlConnection DbConnection = new SqlConnection(strcon);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();

            try
            {
                DbConnection.Open();

                SqlCommand command = new SqlCommand("[PAM].[usp_GetCustomPackageForms]", DbConnection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add("@PackageID", SqlDbType.Int).Value = PackageID;

                da.SelectCommand = command;
                da.Fill(ds);

                DbConnection.Close();
                return ds;
            }
            catch (SqlException ex)
            {
                throw;
            }
        }

        public DataSet GetPackageFormsByName(DataTable PackageFormsTable)
        {
            SqlConnection DbConnection = new SqlConnection(strcon);
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();

            try
            {
                DbConnection.Open();

                SqlCommand command = new SqlCommand("[PAM].[usp_GetPackageFormsByName]", DbConnection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add("@PackageForms", SqlDbType.Structured).Value = PackageFormsTable;

                da.SelectCommand = command;
                da.Fill(ds);

                DbConnection.Close();
                return ds;
            }
            catch (SqlException ex)
            {
                throw;
            }
        }

        public string AddPackageToDB(string Package_Name, string CreatedBy, DataTable FormsTable)
        {
            SqlConnection DbConnection = new SqlConnection(strcon);

            try
            {
                DbConnection.Open();

                SqlCommand command = new SqlCommand("[PAM].[usp_AddFormstoNewPackage]", DbConnection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add("@Package_Name", SqlDbType.NVarChar, 512).Value = Package_Name;
                command.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 100).Value = CreatedBy;
                command.Parameters.Add("@Forms", SqlDbType.Structured).Value = FormsTable;

                SqlParameter returnParameter = command.Parameters.Add("RetVal", SqlDbType.Int);
                returnParameter.Direction = ParameterDirection.ReturnValue;

                int i = command.ExecuteNonQuery();
                int retValue = (int)returnParameter.Value;

                DbConnection.Close();
                if (i >= 1)
                    return "Custom package created successfully";
                else if (retValue == -1)
                    return "Custom package name already exists";
                else
                    return "Custom package could not be added";
            }
            catch (SqlException ex)
            {
                throw;
            }
        }


        public string UpdatePkgFormsToDB(int PackageID, string Package_Name, string CreatedBy, DataTable FormsTable)
        {
            SqlConnection DbConnection = new SqlConnection(strcon);

            try
            {
                DbConnection.Open();

                SqlCommand command = new SqlCommand("[PAM].[usp_UpdateExistingPackage]", DbConnection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add("@PackageID", SqlDbType.NVarChar, 512).Value = PackageID;
                command.Parameters.Add("@Package_Name", SqlDbType.NVarChar, 512).Value = Package_Name;
                command.Parameters.Add("@CreatedBy", SqlDbType.NVarChar, 100).Value = CreatedBy;
                command.Parameters.Add("@Forms", SqlDbType.Structured).Value = FormsTable;

                SqlParameter returnParameter = command.Parameters.Add("RetVal", SqlDbType.Int);
                returnParameter.Direction = ParameterDirection.ReturnValue;

                int i = command.ExecuteNonQuery();
                int retValue = (int)returnParameter.Value;

                DbConnection.Close();

                if (i >= 1)
                    return "Forms are updated successfully to the selected Package";
                else
                    return "Forms could not be updated";
            }
            catch (SqlException ex)
            {
                throw;
            }
        }

        public string DeleteCustomPackagesAndForms(DataTable PackagesAndForms)
        {
            SqlConnection DbConnection = new SqlConnection(strcon);

            try
            {
                DbConnection.Open();

                SqlCommand command = new SqlCommand("[PAM].[usp_DeleteCustomPackagesAndForms]", DbConnection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add("@PackagesAndForms", SqlDbType.Structured).Value = PackagesAndForms;

                //SqlParameter returnParameter = command.Parameters.Add("RetVal", SqlDbType.Int);
                //returnParameter.Direction = ParameterDirection.ReturnValue;

                int i = command.ExecuteNonQuery();
                //int retValue = (int)returnParameter.Value;

                DbConnection.Close();

                if (i >= 1)
                    return "Custom package deleted successfully";
                else
                    return "Custom package could not be deleted";
            }
            catch (SqlException ex)
            {
                throw;
            }
        }
    }
}