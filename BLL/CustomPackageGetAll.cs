﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BLL
{
    public class CustomPackageGetAll
    {
        public int PackageID { get; set; }
        public string Package_Name { get; set; }
        public int FormCount { get; set; }
        public DateTime LastUpdatedDate { get; set; }
        //public bool IsActive { get; set; }        
    }
}