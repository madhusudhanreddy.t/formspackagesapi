﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class EmailContents
    {
        public string recipient { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }

        public List<DefaultPackage> DefaultPkgList { get; set; }
        public PackagesAndFormsNames CustomPackagesForms { get; set; }
        public string[] docNames { get; set; }
    }
}
