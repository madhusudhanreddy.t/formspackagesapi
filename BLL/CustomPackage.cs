﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BLL
{
    public class CustomPackage
    {
        public int PackageID { get; set; }
        public string Package_Name { get; set; }       
        public int FormCount { get; set; }
        public string CreatedBy { get; set; }
        public DateTime LastUpdatedDate { get; set; }
        //public bool IsActive { get; set; }

        public List<Forms> FormsList { get; set; }
    }
}