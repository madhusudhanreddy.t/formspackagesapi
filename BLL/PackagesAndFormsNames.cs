﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class PackagesAndFormsNames
    {
        public List<int> FullPackageIDs { get; set; }
        public List<CustomPackage> PartialPkg { get; set; }
    }
}
