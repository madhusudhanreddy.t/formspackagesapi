﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class DefaultPackage
    {
        public string Package_Name { get; set; }
        public string Version { get; set; }

        public List<Forms> FormsList { get; set; }
    }
}
