﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
//using System.Text;
//using System.Threading.Tasks;
using System.IO.Compression;
using System.IO;
using System.Web.Configuration;

namespace BLL
{
    public class PackageMethods
    {
        //private string Outputlocation = @WebConfigurationManager.AppSettings["Outputlocation"];
        public List<CustomPackageGetAll> GetAllPackages(string CreatedBy)
        {
            FormsPkgDL DBObj = new FormsPkgDL();
            Helper hlpObj = new Helper();
            List<CustomPackageGetAll> PkgList = new List<CustomPackageGetAll>();

            using (DataSet ds = DBObj.GetAllPackages(CreatedBy))
            {
                if (ds.Tables.Count > 0)
                    // Map data to UserGet class using DataTableMapToList
                    PkgList = hlpObj.ConvertDataTable<CustomPackageGetAll>(ds.Tables[0]).ToList();
            }
            return PkgList;
        }

        public CustomPackage GetPackageForms(int PackageID)
        {
            FormsPkgDL DBObj = new FormsPkgDL();
            Helper hlpObj = new Helper();

            CustomPackage CP = new CustomPackage();
            List<Forms> FormsList = new List<Forms>();

            using (DataSet ds = DBObj.GetPackageForms(PackageID))
            {
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                        foreach (DataRow dr in ds.Tables[0].Rows)
                            foreach (PropertyInfo prop1 in CP.GetType().GetProperties())
                                if (!prop1.Name.Equals("FormsList", StringComparison.InvariantCultureIgnoreCase)
                                        && !prop1.Name.Equals("CreatedBy", StringComparison.InvariantCultureIgnoreCase) && !Equals(dr[prop1.Name], DBNull.Value))
                                    prop1.SetValue(CP, dr[prop1.Name], null);
                                else if (prop1.Name.Equals("FormsList", StringComparison.InvariantCultureIgnoreCase))
                                    if (ds.Tables[1].Rows.Count > 0)
                                        foreach (DataRow drPA in ds.Tables[1].Rows)
                                        {
                                            var objForms = Activator.CreateInstance<Forms>();
                                            foreach (PropertyInfo prop2 in objForms.GetType().GetProperties())
                                                if (!Equals(drPA[prop2.Name], DBNull.Value))
                                                    prop2.SetValue(objForms, drPA[prop2.Name], null);
                                            FormsList.Add(objForms);
                                        }
                }
                CP.FormsList = FormsList;
            }
            return CP;
        }

        //public PackagesAndFormsNames ExtractPackageFormsFromdocNames(string[] formNames)
        //{
        //    FormsPkgDL DBObj = new FormsPkgDL();
        //    PackagesAndFormsNames FP = new PackagesAndFormsNames();

        //    DataTable FormsTable = new DataTable();
        //    DataSet PackageFormsTable = new DataSet();

        //    FormsTable.Columns.Add("Form_Name", typeof(string));

        //    foreach (var formName in formNames)
        //    {
        //        DataRow dr = FormsTable.NewRow();
        //        dr["Form_Name"] = formName;
        //        FormsTable.Rows.Add(dr);
        //    }

        //    PackageFormsTable = DBObj.GetPackageFormsByName(FormsTable);

        //    if (PackageFormsTable.Tables[0].Rows.Count > 0)
        //        foreach (DataRow dr in PackageFormsTable.Tables[0].Rows)
        //            foreach (PropertyInfo prop in FP.GetType().GetProperties())
        //                if (!Equals(dr[prop.Name], DBNull.Value))
        //                    prop.SetValue(FP, dr[prop.Name], null);
        //    return FP;
        //}

        public string AddCustomPackage(CustomPackage Pkg)
        {
            FormsPkgDL DBObj = new FormsPkgDL();
            DataTable FormsTable = new DataTable();

            FormsTable.Columns.Add("Form_Name", typeof(string));

            foreach (var form in Pkg.FormsList)
            {
                DataRow dr = FormsTable.NewRow();
                foreach (PropertyInfo prop in form.GetType().GetProperties())
                {
                    if (!prop.Name.Equals("Form_AddDate", StringComparison.InvariantCultureIgnoreCase) && form.GetType().GetProperty(prop.Name).GetValue(form, null) != null)
                        dr[prop.Name] = form.GetType().GetProperty(prop.Name).GetValue(form, null);
                }
                FormsTable.Rows.Add(dr);
            }

            string retval = DBObj.AddPackageToDB(Pkg.Package_Name, Pkg.CreatedBy, FormsTable);

            return retval;
        }

        public string UpdateCustomPackage(CustomPackage Pkg)
        {
            FormsPkgDL DBObj = new FormsPkgDL();
            DataTable FormsTable = new DataTable();

            FormsTable.Columns.Add("Form_Name", typeof(string));

            foreach (var form in Pkg.FormsList)
            {
                DataRow dr = FormsTable.NewRow();
                foreach (PropertyInfo prop in form.GetType().GetProperties())
                {
                    if (!prop.Name.Equals("Form_AddDate", StringComparison.InvariantCultureIgnoreCase) && form.GetType().GetProperty(prop.Name).GetValue(form, null) != null)
                        dr[prop.Name] = form.GetType().GetProperty(prop.Name).GetValue(form, null);
                }
                FormsTable.Rows.Add(dr);
            }

            string retval = DBObj.UpdatePkgFormsToDB(Pkg.PackageID, Pkg.Package_Name, Pkg.CreatedBy, FormsTable);

            return retval;
        }

        public string DeleteMultiPackagesForms(List<CustomPackage> PackageFormsList)
        {
            FormsPkgDL DBObj = new FormsPkgDL();
            DataTable PkgFormsTable = new DataTable();

            PkgFormsTable.Columns.Add("PackageID", typeof(int));
            PkgFormsTable.Columns.Add("Form_Name", typeof(string));
                       
            foreach (var pkg in PackageFormsList)
            {
                if (pkg.FormsList == null)
                {
                    DataRow dr = PkgFormsTable.NewRow();
                    dr["PackageID"] = pkg.PackageID;

                    PkgFormsTable.Rows.Add(dr);
                }
                else
                {
                    foreach (var form in pkg.FormsList)
                    {
                        DataRow dr = PkgFormsTable.NewRow();
                        dr["PackageID"] = pkg.PackageID;
                        dr["Form_Name"] = form.Form_Name;

                        PkgFormsTable.Rows.Add(dr);
                    }
                }
            }

            string retval = DBObj.DeleteCustomPackagesAndForms(PkgFormsTable);

            return retval;
        }

        public void zipFiles(string @FolderPath)
        {
            DirectoryInfo tempDirInfo = new DirectoryInfo(@FolderPath);
            string dlForms = "DownloadedForms";
            string zipFileName = DateTime.UtcNow.ToString("yyyyMMddHHmmss") + dlForms;

            foreach (var zipfile1 in tempDirInfo.GetFiles())
            {
                if (zipfile1.Extension == ".zip" && zipfile1.Name.Contains(dlForms))
                    zipfile1.Delete();
            }

            DirectoryInfo appDirInfo = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory + @"\TempForms\");

            foreach (var zipfile2 in appDirInfo.GetFiles())
            {
                zipfile2.Delete();
            }

            string DestinationFilePath = @FolderPath + @"\" + zipFileName + ".zip";

            // To avoid file access lock issues the zipping is done in two steps as shown below
            //  1. Create zip file of files from TLB share path to WebAPI application directory base path.
            ZipFile.CreateFromDirectory(@FolderPath, AppDomain.CurrentDomain.BaseDirectory + @"\TempForms\" + zipFileName + ".zip");
            //  2. Move file from WebAPI application directory base path back to TLB share path.
            File.Move(AppDomain.CurrentDomain.BaseDirectory + @"\TempForms\" + zipFileName + ".zip", DestinationFilePath);
        }

        public string GetFileSavePath(string fileSavePath)
        {
            try
            {
                if (Directory.Exists(fileSavePath))
                {
                    DirectoryInfo dirInfo = new DirectoryInfo(fileSavePath);

                    foreach (var file in dirInfo.GetFiles())
                    {
                        file.Delete();
                    }
                }
                else
                    Directory.CreateDirectory(fileSavePath);

                return fileSavePath;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void DeleteUserFolder(string LaserfischeSharedlocation)
        {
            try
            {
                if (Directory.Exists(LaserfischeSharedlocation))
                {
                    Directory.Delete(LaserfischeSharedlocation, true);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void DeletePolicyPDF(string PolicyPDFlocation)
        {
            try
            {
                if (Directory.Exists(PolicyPDFlocation))
                {
                    string[] files = System.IO.Directory.GetFiles(@PolicyPDFlocation, "*PolicyDetails.pdf");

                    foreach (string file in files)
                    {
                        System.IO.File.Delete(file);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
